Feature: Test Create methods in Continent REST API testing
  Scenario: Add Continent record
    Given I Set POST api endpoint continent
    When Send a POST HTTP request continent
    Then I receive POST valid Response continent
    And I receive POST valid HTTP response code 201 continent

 Scenario: Get ALL Continent record
    Given I Set endpoint api get all continent
    When Send GET HTTP request api all continent
    Then I receive GET valid HTTP response code 200 api all continent
    
  Scenario: Get By Id Continent record
    Given I Set endpoint api get continent by id
    When Send GET HTTP request api continent by id
    Then I receive GET valid HTTP response code 200 api continent by id
    
  Scenario: Update Continent record
    Given I Set POST api update continent
    When Send a POST HTTP request update continent
    Then I receive POST valid Response update continent
    And I receive POST valid HTTP response code 200 update continent
    
  Scenario: Delete Continent record
    Given I Set endpoint api delete continent by id
    When Send DELETE HTTP request api delete continent by id
    Then I receive DELETE valid HTTP response code 200 api delete continent by id