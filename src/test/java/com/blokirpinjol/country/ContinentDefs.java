package com.blokirpinjol.country;
   
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;  
import java.io.IOException; 
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert; 
import com.fasterxml.jackson.databind.JsonNode;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class ContinentDefs extends SpringIntegrationTest{  
//	SAVE CONTINENT
	@Given("^I Set POST api endpoint continent$")
    public void setPostEndpointDeviceContinent(){
        addURI = "http://localhost:8081/api/continent";
        System.out.println("Add URL :"+addURI);
    }
	
	@When ("^Send a POST HTTP request continent$")
    public void sendPostRequestContinent(){ 
		try {  
			executePost(addURI,"{\"name\":\"Indonesia\"}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Then("^I receive POST valid Response continent$")
	public void cekPostBodyContinent() throws Throwable { 
		JsonNode root = objectMapper.readTree(latestResponse.getBody());  
		id = root.path("id").asText(); 
		System.out.print(id);
	    assertThat(root.path("name").asText(), is("Indonesia"));
	}

	@And("^I receive POST valid HTTP response code (\\d+) continent$")
	public void cekPostStatusContinent(int statusCode) throws Throwable {
		HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode(); 
        System.out.println("Add URL :"+currentStatusCode.value());
        assertThat(currentStatusCode.value(), is(statusCode));
		Assert.isTrue(currentStatusCode.value()==statusCode);
	}
	
//GET ALL CONTINENT
	@Given("^I Set endpoint api get all continent$")
    public void setGETEndpointGetAllContinent(){
        addURI = "http://localhost:8081/api/continent";
        System.out.println("Add URL :"+addURI);
    }
	
	@When ("^Send GET HTTP request api all continent$")
    public void sendGETRequestGetAllContinent(){ 
		try {
			executeGet(addURI);
		} catch (IOException e) { 
			e.printStackTrace();
		}
    }
	
	@Then("^I receive GET valid HTTP response code (\\d+) api all continent$")
	public void cekGETStatusGetAllContinent(int statusCode) throws Throwable { 
		HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();  
        assertThat(currentStatusCode.value(), is(statusCode)); 
	}

	//GET By ID CONTINENT
	@Given("^I Set endpoint api get continent by id$")
    public void setGETEndpointGetByIdContinent(){
        addURI = "http://localhost:8081/api/continent/" + id;
        System.out.println("Add URL :"+addURI);
    }
	
	@When ("^Send GET HTTP request api continent by id$")
    public void sendGETRequestGetByIdContinent(){ 
		try {
			executeGet(addURI);
		} catch (IOException e) { 
			e.printStackTrace();
		}
    }
	
	@Then("^I receive GET valid HTTP response code (\\d+) api continent by id$")
	public void cekGETStatusGetByIdContinent(int statusCode) throws Throwable { 
		HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();  
        assertThat(currentStatusCode.value(), is(statusCode)); 
	}
	

//	UPDATE CONTINENT
	@Given("^I Set POST api update continent$")
    public void setPostEndpointUpdateContinent(){
        addURI = "http://localhost:8081/api/continent/update/"+id;
        System.out.println("Add URL :"+addURI);
    }
	
	@When ("^Send a POST HTTP request update continent$")
    public void sendPostRequestUpdateContinent(){ 
		try {
			executePost(addURI,"{\"name\":\"Indonesia12\"}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Then("^I receive POST valid Response update continent$")
	public void cekPostBodyUpdateContinent() throws Throwable { 
		JsonNode root = objectMapper.readTree(latestResponse.getBody());   
	    assertThat(root.path("name").asText(), is("Indonesia12")); 
	}

	@And("^I receive POST valid HTTP response code (\\d+) update continent$")
	public void cekPostStatusUpdateContinent(int statusCode) throws Throwable {
		HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode(); 
        System.out.println("Add URL :"+currentStatusCode.value());
        assertThat(currentStatusCode.value(), is(statusCode));
		Assert.isTrue(currentStatusCode.value()==statusCode);
	}
	
//DELETE CONTINENT 
	@Given("^I Set endpoint api delete continent by id$")
    public void setDELETEEndpointContinent(){
		addURI = "http://localhost:8081/api/continent/delete/" + id;
        System.out.println("Add URL :"+addURI);
    }
	
	@When ("^Send DELETE HTTP request api delete continent by id$")
    public void sendDELETERequestContinent(){ 
		try {
			executePost(addURI, "");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Then("^I receive DELETE valid HTTP response code (\\d+) api delete continent by id$")
	public void cekDELETEStatusContinent(int statusCode) throws Throwable { 
		HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();  
        assertThat(currentStatusCode.value(), is(statusCode)); 
	} 
}
