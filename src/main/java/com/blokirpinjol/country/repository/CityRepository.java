package com.blokirpinjol.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blokirpinjol.country.model.City; 

public  interface CityRepository extends JpaRepository<City, Integer>  {

}
