package com.blokirpinjol.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blokirpinjol.country.model.District;

public interface DistrictRepository  extends JpaRepository<District, Integer> {

}
