package com.blokirpinjol.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blokirpinjol.country.model.Village;

public interface VillageRepository  extends JpaRepository<Village, Integer> {

}
