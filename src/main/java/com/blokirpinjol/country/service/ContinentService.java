package com.blokirpinjol.country.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blokirpinjol.country.model.Continent;
import com.blokirpinjol.country.repository.ContinentRepository; 

@Service
public class ContinentService {
	
Logger logger = LoggerFactory.getLogger(ContinentService.class);
    
	@Autowired
	private ContinentRepository repository;
	
	public Continent save(Continent continent) {
		continent.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return repository.save(continent);
	}
	
	public List<Continent> getAll(){
		return repository.findAll();
	}
	
	public Optional<Continent> getByid(Integer id) {
		return repository.findById(id);
	}
	
	public Continent update(Integer id,Continent continent) {
		Optional<Continent> continentcek = repository.findById(id);
		if(continentcek.isPresent()) { 
			if(continent.getName() != null ) continentcek.get().setName(continent.getName());
			continentcek.get().setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return repository.save(continentcek.get()); 
		}else {
			return continentcek.get();
		}
	}
	
	public Boolean delete(Integer id) {
		Optional<Continent> continent = repository.findById(id);
		if(continent.isPresent()) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}
}
