package com.blokirpinjol.country.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blokirpinjol.country.model.Village;
import com.blokirpinjol.country.repository.VillageRepository; 

@Service
public class VillageService {
	
Logger logger = LoggerFactory.getLogger(VillageService.class);
    
	@Autowired
	private VillageRepository repository;
	
	public Village save(Village village) {
		village.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return repository.save(village);
	}
	
	public List<Village> getAll(){
		return repository.findAll();
	}
	
	public Optional<Village> getByid(Integer id) {
		return repository.findById(id);
	}
	
	public Village update(Integer id,Village village) {
		Optional<Village> villagecek = repository.findById(id);
		if(villagecek.isPresent()) { 
			if(village.getName() != null ) villagecek.get().setName(village.getName());
			if(village.getDistrict_id() != null ) villagecek.get().setDistrict_id(village.getDistrict_id());
			villagecek.get().setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return repository.save(villagecek.get()); 
		}else {
			return villagecek.get();
		}
	}
	
	public Boolean delete(Integer id) {
		Optional<Village> village = repository.findById(id);
		if(village.isPresent()) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}
}
