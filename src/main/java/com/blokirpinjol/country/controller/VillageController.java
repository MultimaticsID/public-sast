package com.blokirpinjol.country.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blokirpinjol.country.model.Village;
import com.blokirpinjol.country.service.VillageService;

@RestController
@RequestMapping("/api/village")
public class VillageController {
	Logger logger = LoggerFactory.getLogger(VillageController.class);
    
    @Autowired
    private VillageService service;
    
	@PostMapping
    public ResponseEntity<Village> saveCity(@RequestBody Village village) {
		Village hasil = new Village();
    	try {
    		hasil = service.save(village);
    		return new ResponseEntity<Village>(hasil, HttpStatus.CREATED);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Village>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    }  
	
	@GetMapping
    public ResponseEntity<List<Village>> getAllCity() {
		List<Village> data = new ArrayList<Village>();
    	try {
    		data = service.getAll();
    		return new ResponseEntity<List<Village>>(data, HttpStatus.OK);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<List<Village>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@GetMapping("/{id}")
    public ResponseEntity<Optional<Village>> getByIdCity(@PathVariable Integer id) {
		Optional<Village> data = null;
    	try {
    		data = service.getByid(id);
    		if(data.isPresent()) {
        		return new ResponseEntity<Optional<Village>>(data, HttpStatus.OK);
    		}else { 
        		return new ResponseEntity<Optional<Village>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Optional<Village>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, Object>> deleteByIdCity(@PathVariable Integer id) {
		Map<String, Object> data =  new HashMap();
		Boolean hasil = false;
    	try {
    		hasil = service.delete(id);
    		if(hasil) {
    			data.put("message", "Deleted Successfully");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.OK);
    		}else { 
    			data.put("message", "Data Not Found");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
			data.put("message", e.getMessage());
    		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/update/{id}")
    public ResponseEntity<Village> updateCity(@PathVariable Integer id, @RequestBody Village village) {
		Village hasil = new Village();
    	try {
    		hasil = service.update(id,village);
    		if(hasil != null) { 
        		return new ResponseEntity<Village>(hasil, HttpStatus.OK);
    		}else {  
        		return new ResponseEntity<Village>(hasil, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Village>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 

}
